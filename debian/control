Source: ergo
Section: science
Priority: optional
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders: Michael Banck <mbanck@debian.org>
Build-Depends: bc,
               debhelper (>= 12),
               libatlas-base-dev,
               libblas-dev,
               liblapack-dev
Standards-Version: 3.9.4
Homepage: http://ergoscf.org/
Vcs-Browser: https://salsa.debian.org/debichem-team/ergo
Vcs-Git: https://salsa.debian.org/debichem-team/ergo.git

Package: ergo
Architecture: any
Depends: ergo-data (>= 3.7), ${misc:Depends}, ${shlibs:Depends}
Description: Quantum chemistry program for large-scale calculations
 ErgoSCF is a quantum chemistry program for large-scale self-consistent field
 calculations.  It employs modern linear scaling techniques like fast multipole
 methods, hierarchic sparse matrix algebra, density matrix purification, and
 efficient integral screening.  Linear scaling is achieved not only in terms of
 CPU usage but also memory utilization.  It uses Gaussian basis sets.
 .
 It can compute single-point energies for the following methods:
  * Restricted and unrestricted Hartree-Fock (HF) theory
  * Restricted and unrestricted Kohn-Sham density functional theory (DFT)
  * Full Configuration-Interaction (FCI)
 .
 The following Exchange-Correlational (XC) density functionals are included:
  * Local Density Approximation (LDA)
  * Gradient-corrected (GGA) XC functionals BLYP, BP86, PW91 and PBE
  * Hybrid XC functionals B3LYP, BHandHLYP, PBE0 and CAMB3LYP
 .
 Further features include:
  * Linear response calculations (polarizabilities and excitation energies) for
    restricted reference densities
  * External electric fields
  * Electron dynamics via Time-Dependent Hartree-Fock (TDHF)

Package: ergo-data
Architecture: all
Depends: ${misc:Depends}
Replaces: ergo (<< 3.7)
Breaks: ergo (<< 3.7)
Description: Quantum chemistry program for large-scale calculations - data package
 ErgoSCF is a quantum chemistry program for large-scale self-consistent field
 calculations.  It employs modern linear scaling techniques like fast multipole
 methods, hierarchic sparse matrix algebra, density matrix purification, and
 efficient integral screening.  Linear scaling is achieved not only in terms of
 CPU usage but also memory utilization.  It uses Gaussian basis sets.
 .
 It can compute single-point energies for the following methods:
  * Restricted and unrestricted Hartree-Fock (HF) theory
  * Restricted and unrestricted Kohn-Sham density functional theory (DFT)
  * Full Configuration-Interaction (FCI)
 .
 The following Exchange-Correlational (XC) density functionals are included:
  * Local Density Approximation (LDA)
  * Gradient-corrected (GGA) XC functionals BLYP, BP86, PW91 and PBE
  * Hybrid XC functionals B3LYP, BHandHLYP, PBE0 and CAMB3LYP
 .
 Further features include:
  * Linear response calculations (polarizabilities and excitation energies) for
    restricted reference densities
  * External electric fields
  * Electron dynamics via Time-Dependent Hartree-Fock (TDHF)
 .
 This package contains data for ergo.
